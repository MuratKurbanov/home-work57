const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

let time = tasks.reduce((totalTime,currentValue) => {
    if(currentValue.category === 'Frontend') {
        totalTime += currentValue.timeSpent
    }
    return totalTime
}, 0);

console.log('Total time Frontend' + ' ' + time);

let times = tasks.reduce((totalTimes, currentValue) => {
   if(currentValue.type === 'bug') {
       totalTimes += currentValue.timeSpent
   }
   return totalTimes
}, 0);

console.log('Total time bug' + ' ' + times);

const tasksWithUi = tasks.filter(task => task.title.includes('UI')).length;

console.log(tasksWithUi);

let all = tasks.reduce((AllTask, currentValue) => {
    if(currentValue.category === 'Backend') {
        AllTask.Frontend++
    }
    if(currentValue.category === 'Frontend') {
        AllTask.Backend++
    }
    return AllTask
},{Frontend: 0, Backend: 0});

console.log(all);

let task = tasks.reduce((totalTimes, currentValue) => {
    if(currentValue.timeSpent > 4) {
        totalTimes.push(currentValue.title + ' ' + currentValue.category)
    }
    return totalTimes
}, []);

console.log(task);

